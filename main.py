import os
from configuration import configuration
from magnitudes import magni
from observations import obs


dir, acos, obs_old, obs_new = config()
os.chdir(dir)

magni(dir,acos)

obs(dir,obs_old)
obs(dir,obs_new)


