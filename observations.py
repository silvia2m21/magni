import os
import numpy as np
import jdcal
from astroquery.jplhorizons import Horizons


# Generates tables for plots of reduced magnitude vs. heliocentric distance for our observations


def obs(dir,obsfile):

    files = (file for file in os.listdir(dir)
             if os.path.isfile(os.path.join(dir, file)))

    for file in files:

        os.chdir(os.path.join(dir,file + '_dir'))

        obs = obsfile[-14:].replace('.txt', '')

        if os.path.exists(file + '_' + obs):
            os.remove(file + '_' + obs)

        with open(obsfile,'r') as f1:

            for ln in f1:

                year = ln[10:15]
                month = ln[15:18]
                day = ln[18:21]
                mag = ln[23:28]

                date = int(sum(jdcal.gcal2jd(int(year), int(month), int(day))))+0.5

                name = ln[29:37].replace('\n','').replace(' ','')
                ast = ln[0:9].replace(' ','')

                if len(name) > 6:
                    ast = ast[0:4] + ' ' + ast[4:]

                if name == file:

                    with open(name + '_' + obs,'a') as f2:

                        print(name, ast)

                        obj = Horizons(id=ast, location='500', epochs=date)
                        eph = obj.ephemerides()

                        Delta = "%6.3f" % eph['delta'][0]   # Geocentric distance
                        r = "%6.3f" % eph['r'][0]           # Heliocentric distance
                        rdot = eph['r_rate'][0]             # Heliocentric rate
                        alpha = "%5.2f" % eph['alpha'][0]   # Phase angle

                        if float(rdot) > 0:
                            IO = -1             # After perihelion
                        else:
                            IO = "%2.0f" % 1    # Before perihelion

                        RM = float(float(mag) - 5 * np.log10(float(r) * float(Delta)) - 0.04 * float(alpha))
                        RM = "%5.2f" % RM

                        f2.write(str(r) + ' ' + str(RM) + ' ' + str(IO) + '\n')
