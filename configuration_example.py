
def config():

    dir = '/home/username/magni/'
    acos = '/home/username/results/aster/elements/elem_acos'
    obs_old = '/home/username/magni/result_old.txt'		# Data from 2015-2016 observations
    obs_new = '/home/username/magni/result_new.txt'		# Data from 2019-2021 observations

    return dir, acos, obs_old, obs_new
