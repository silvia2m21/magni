close all
clc

% Generates png magni plots

name = 'name' 	   % Object name (MPC)

file = strcat(name,'_m.txt');
qQH = load(strcat(name,'_qQH.txt'));
rRM = load(file);
ast = file(1:strfind(file,'_')-1);

r = rRM(:,5);      % Heliocentric distance
RM = rRM(:,7);     % Reduced magnitude
BS = rRM(:,10);    % Big survey = 1
IO = rRM(:,8);     % In/Out perihelion. Pre = 1, Post = -1
q = qQH(1);        % Perihelion distance
Q = qQH(2);        % Afelioin distance
H = qQH(3);        % Absolute magnitude (MPC)


% Separates data in Big Survey/Common

r_B = zeros(length(BS),1);
RM_B = zeros(length(BS),1);
r_C = zeros(length(BS),1);
RM_C = zeros(length(BS),1);

k = 1;
h = 1;
for i = 1:length(BS)
    if BS(i) == 1
        r_B(k) = r(i); RM_B(k) = RM(i);
        k = k + 1;
    else
        r_C(h) = r(i); RM_C(h) = RM(i);
        h = h + 1;        
    end
end

for j = length(BS):-1:1
    if r_B(j) == 0
        r_B(j) = []; RM_B(j) = [];
    end
    if r_C(j) == 0
        r_C(j) = []; RM_C(j) = [];
    end
end


% Separates data in In/Out perihelion

r_BI = zeros(length(r_B),1);
RM_BI = zeros(length(r_B),1);
r_BO = zeros(length(r_B),1);
RM_BO = zeros(length(r_B),1);
r_CI = zeros(length(r_C),1);
RM_CI = zeros(length(r_C),1);
r_CO = zeros(length(r_C),1);
RM_CO = zeros(length(r_C),1);
        
k = 1;
h = 1;
for i = 1:length(RM_B)
    if IO(i) == 1
        r_BI(k) = r_B(i); RM_BI(k) = RM_B(i);
        k = k + 1;
    else
        r_BO(h) = r_B(i); RM_BO(h) = RM_B(i);
        h = h + 1;        
    end
end

for j = length(RM_B):-1:1
    if r_BI(j) == 0
        r_BI(j) = []; RM_BI(j) = [];       
    end
    if r_BO(j) == 0
        r_BO(j) = []; RM_BO(j) = [];
    end
end

k = 1;
h = 1;
for i = 1:length(RM_C)
    if IO(i) == 1
        r_CI(k) = r_C(i); RM_CI(k) = RM_C(i);
        k = k + 1;
    else
        r_CO(h) = r_C(i); RM_CO(h) = RM_C(i);
        h = h + 1;        
    end
end

for j = length(RM_C):-1:1
    if r_CI(j) == 0
        r_CI(j) = []; RM_CI(j) = [];
    end
    if r_CO(j) == 0
        r_CO(j) = []; RM_CO(j) = [];
    end
end


% Plot all data

if Q>max(r)+3 && min(r) < q +10
    xlimit1 = max(r)+2;
else    
    xlimit1 = Q+1;
end

figure(1)
xlim([q-1,xlimit1])
ylim([min(RM)-2,max(RM)+2])
plot(r_CO,RM_CO,'o','MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor','k')
hold on
plot(r_BO,RM_BO,'^','MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor','k')
plot(r_CI,RM_CI,'ok','MarkerSize',6)
plot(r_BI,RM_BI,'^k','MarkerSize',6)
plot([q,q],[min(ylim()),max(ylim())], 'k')
plot([Q,Q],[min(ylim()),max(ylim())], 'k')
xlabel('r (au)','FontSize',14), ylabel('Reduced magnitude','FontSize',14)
plot([min(xlim()),max(xlim())],[H,H], 'k--')
set(gca, 'YDir','reverse','FontSize',14)
title(name,'FontSize',14,'FontWeight','bold')


% Plot big surveys data

try  
    if Q>max(r_B)+3
        xlimit2 = max(r_B)+2;
    else    
        xlimit2 = Q+1;
    end
    
    if not(isempty(RM_B))
    
        figure(2)
        hold on
        xlim([q-1,xlimit2])
        ylim([min(RM_B)-2,max(RM_B)+2])
        plot(r_BO,RM_BO,'o','MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor','k')
        plot(r_BI,RM_BI,'ok','MarkerSize',6)
        xlabel('r (au)','FontSize',14'), ylabel('Reduced magnitude','FontSize',14)
        plot([min(xlim()),max(xlim())],[H,H], 'k--')
        set(gca, 'YDir','reverse','FontSize',14)
    end
catch
    disp('There are no big surveys observations')
end


% Adds our observations

try
    obs = load(strcat(name,'_result_new'));
    r_obs = obs(:,1);
    RM_obs = obs(:,2);
    IO_obs = obs(:,3);
    robs_I = zeros(length(r_obs),1);
    RMobs_I = zeros(length(r_obs),1);
    robs_O = zeros(length(r_obs),1);
    RMobs_O = zeros(length(r_obs),1);
    
    k = 1;
    h = 1;
    for i = 1:length(RM_obs)
        if IO_obs(i) == 1
            robs_I(k) = r_obs(i); RMobs_I(k) = RM_obs(i);
            k = k + 1;
        else
            robs_O(h) = r_obs(i); RMobs_O(h) = RM_obs(i);
            h = h + 1;        
        end
    end

    for j = length(RM_obs):-1:1
        if robs_I(j) == 0
            robs_I(j) = []; RMobs_I(j) = [];       
        end
        if robs_O(j) == 0
            robs_O(j) = []; RMobs_O(j) = [];
        end
    end
    
    if max(r_obs)>max(r)
        xlimit1 = max(r_obs)+2;
        xlimit2 = max(r_obs)+2;
    end
    
    if min(RM_obs) < min(RM)
        ylimit = min(RM_obs)-2;        
    else
        ylimit = min(RM)-2;
    end
            
    figure(1)
    plot(robs_O,RMobs_O,'s','MarkerSize',6,'MarkerEdgeColor','r','MarkerFaceColor','r')
    hold on
    plot(robs_I,RMobs_I,'sr','MarkerSize',6)
    xlim([q-1,xlimit1])
    ylim([ylimit,max(RM)+2])
    plot([q,q],[min(ylim()),max(ylim())], 'k')
    plot([Q,Q],[min(ylim()),max(ylim())], 'k')
    plot([min(xlim()),max(xlim())],[H,H], 'k--')

    if not(isempty(RM_B))
        figure(2)
        plot(robs_O,RMobs_O,'s','MarkerSize',6,'MarkerEdgeColor','r','MarkerFaceColor','r')
        hold on
        plot(robs_I,RMobs_I,'sr','MarkerSize',6)
        xlim([q-1,xlimit2])
        ylim([ylimit,max(RM_B)+2])
        plot([q,q],[min(ylim()),max(ylim())], 'k')
        plot([Q,Q],[min(ylim()),max(ylim())], 'k')            
        plot([min(xlim()),max(xlim())],[H,H], 'k--')
    end
    
catch
    disp('There are no new results')
end

try
    old = load(strcat(name,'_result_old'));
    r_old = old(:,1);
    RM_old = old(:,2);
    IO_old = old(:,3);
    rold_I = zeros(length(r_old),1);
    RMold_I = zeros(length(r_old),1);
    rold_O = zeros(length(r_old),1);
    RMold_O = zeros(length(r_old),1);
    
    h = 1;
    for i = 1:length(RM_old)
        if IO_old(i) == 1
            rold_I(k) = r_old(i); RMold_I(k) = RM_old(i);
            k = k + 1;
        else
            rold_O(h) = r_old(i); RMold_O(h) = RM_old(i);
            h = h + 1;        
        end
    end

    for j = length(RM_old):-1:1
        if rold_I(j) == 0
            rold_I(j) = []; RMold_I(j) = [];       
        end
        if rold_O(j) == 0
            rold_O(j) = []; RMold_O(j) = [];
        end
    end  
    
    if max(r_old)>max(r)
        xlimit1 = max(r_old)+2;
        xlimit2 = max(r_old)+2;
    end
    
    try
        if max(r_old)>max(r) && max(r_old) > max(r_obs) && abs(max(r_old)-Q)>2
            xlimit1 = max(r_old)+2;
            xlimit2 = max(r_old)+2;
        elseif max(r_obs)>max(r) && max(r_old) < max(r_obs) && abs(max(r_obs)-Q)>2
            xlimit1 = max(r_obs)+2
            xlimit2 = max(r_obs)+2
        end
    catch
        disp('')
        
    end
    
    if min(RM_old) < min(RM)
        ylimit = min(RM_old)-2;
    end
    
    try
        if min(RM_old) < min(RM) && min(RM_old) < min(RM_obs)
            ylimit = min(RM_old)-2;
        elseif min(RM_obs) < min(RM) && min(RM_old) > min(RM_obs)
            ylimit = min(RM_obs)-2;
        end
    catch
        disp('')
    end
    
    figure(1)
    plot(rold_O,RMold_O,'s','MarkerSize',6,'MarkerEdgeColor','b','MarkerFaceColor','b')
    hold on
    plot(rold_I,RMold_I,'sb','MarkerSize',6)
    xlim([q-1,xlimit1])
    ylim([ylimit,max(RM)+2])
    plot([q,q],[min(ylim()),max(ylim())], 'k')
    plot([Q,Q],[min(ylim()),max(ylim())], 'k')
    plot([min(xlim()),max(xlim())],[H,H], 'k--')

    if not(isempty(RM_B))
        
        figure(2)
        plot(rold_O,RMold_O,'s','MarkerSize',6,'MarkerEdgeColor','b','MarkerFaceColor','b')
        hold on
        plot(rold_I,RMold_I,'sb','MarkerSize',6)
        xlim([q-1,xlimit2])
        ylim([ylimit,max(RM_B)+2])
        plot([q,q],[min(ylim()),max(ylim())], 'k')
        plot([Q,Q],[min(ylim()),max(ylim())], 'k')
        plot([min(xlim()),max(xlim())],[H,H], 'k--')
	title(name,'FontSize',14,'FontWeight','bold')
    end
    
catch
    disp('There are no old results')
end


% Saves png

fig1 =  figure(1);
saveas(fig1,[ast '_all.png'])

if not(isempty(RM_B))
    fig2 = figure(2);
    saveas(fig2,[ast '_surveys.png'])
end

