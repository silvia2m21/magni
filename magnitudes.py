import os
import re
import numpy as np
import jdcal
import shutil
from astroquery.jplhorizons import Horizons


# Generates tables for plots of reduced magnitude vs. heliocentric distance for ACOs


def magni(dir,acos):

	# Writes files with q, Q and H for each object

	files = (file for file in os.listdir(dir)
		 if os.path.isfile(os.path.join(dir, file)))

	for file in files:

	    if os.path.exists(file + '_dir'):
		shutil.rmtree(file + '_dir')

	    os.mkdir(file + '_dir')
	    os.chdir(os.path.join(dir,file + '_dir'))

	    with open(acos, 'r') as f:
		with open(file + '_qQH.txt', 'a') as f1:

		    for ln in f:
		        sp = re.sub(' +', ' ', ln)
		        sb = sp.split(' ')

		        if sb[0] == '':
		            if file == sb[12].replace('\n',''):
		                q = "%6.3f" % float(float(sb[2])*(1-float(sb[3])))
		                Q = "%7.3f" % float(float(sb[2])*(1+float(sb[3])))
		                H = "%5.2f" % float(sb[9])
		                f1.write(q + ' ' + Q + ' ' + H + '\n')
		        else:
		            if file == sb[11].replace('\n',''):
		                q = "%6.3f" % float(float(sb[1])*(1-float(sb[2])))
		                Q = "%7.3f" % float(float(sb[1])*(1+float(sb[2])))
		                H = "%5.2f" % float(sb[8])
		                f1.write(q + ' ' + Q + ' ' + H + '\n')

	    os.chdir(dir)


	# Writes file with year, month, day, delta, r, alfa, RM, I/O, mag, Obs code

	files = (file for file in os.listdir(dir)
		 if os.path.isfile(os.path.join(dir, file)))

	for file in files:
	    print(file)

	    with open(acos, 'r') as f:
		for ln in f:
		    sp = re.sub(' +', ' ', ln)
		    sb = sp.split(' ')

		    if sb[0] == '':
		        if sb[12].replace('\n','') == file:
		            name = sb[11]
		    else:
		        if sb[11].replace('\n','') == file:
		            if len(sb[11])<7:
		                name = sb[10]
		            else:
		                name = sb[10][0:4] + ' ' + sb[10][4:]

	    os.chdir(file + '_dir')

	    if os.path.exists(file + '.txt'):
		os.remove(file + '.txt')

	    if os.path.exists(file + '_m.txt'):
		os.remove(file + '_m.txt')

	    with open(file + '.txt', 'a') as fi:
		with open(file + '_m.txt','a') as fi2:

		    with open(os.path.join(dir,file),'r') as f:

		        for ln in f:
		            sp = re.sub(' +', ' ', ln)
		            sb = sp.split(' ')

		            if ln[65:70] != '     ':
		                if ln[14] != 's' and ln[14] != 'S' and ln[14] != 'r' and ln[14] != 'R':

		                    try:
		                        year = ln[15:19]
		                        month = ln[20:22]
		                        day = ln[23:31]
		                        RA1 = ln[32:34]
		                        RA2 = ln[35:37]
		                        RA3 = ln[38:44]
		                        DEC1 = ln[44:47]
		                        DEC2 = ln[48:50]
		                        DEC3 = ln[51:56]
		                        mag = ln[65:70]                      # Apparent magnitude
		                        Cod = ln[77:81].replace('\n','')     # Observatory code

		                        RAd = (float(RA1) + float(RA2)/60 + float(RA3)/3600)*15             # RA in degrees
		                        DECd = float(DEC1) + float(DEC2)/60 + float(DEC3)/3600              # DEC in degrees
		                        date = int(sum(jdcal.gcal2jd(int(year), int(month), int(np.floor(float(day)))))) +\
		                               np.abs(float(day) - int(float(day)))

		                        obj = Horizons(id=name, location='500', epochs=date)
		                        eph = obj.ephemerides()

		                        RA = eph['RA'][0]                   # RA
		                        DEC = eph['DEC'][0]                 # DEC
		                        Delta = "%6.3f" % eph['delta'][0]   # Geocentric distance
		                        r = "%6.3f" % eph['r'][0]           # Heliocentric distance
		                        rdot = eph['r_rate'][0]             # Heliocentric rate
		                        alpha = "%5.2f" % eph['alpha'][0]   # Phase angle

		                        if float(rdot) > 0:
		                            IO = -1             # After perihelion
		                        else:
		                            IO = "%2.0f" % 1    # Before perihelion


		                        # Computes reduced magnitude (RM) and changes color to a common band V

		                        RM = float(float(mag) - 5*np.log10(float(r)*float(Delta))- 0.04*float(alpha))
		                        mag = "%5.2f" % float(mag)

		                        if ln[70] == ' ' and Cod == '704':
		                            F = 'L'
		                        elif ln[70] == ' ' and Cod == '644':
		                            F = 'R'
		                        elif ln[70] != ' ':
		                            F = ln[70]
		                        else:
		                            F = 'V'

		                        if F == 'B':
		                            RM = RM -0.65
		                        elif F == 'R':
		                            RM = RM +0.36
		                        elif F == 'C':
		                            RM = RM +0.4
		                        elif F == 'I':
		                            RM = RM +0.70
		                        elif F == 'g':
		                            RM = RM -0.21
		                        elif F == 'r':
		                            RM = RM +0.19
		                        elif F == 'i':
		                            RM = RM +0.28
		                        elif F == 'w':
		                            RM = RM +0.11
		                        elif F == 'Z':
		                            RM = RM +0.89
		                        elif F == 'Y':
		                            RM = RM +1.03
		                        elif F == 'J':
		                            RM = RM +1.21
		                        elif F == 'H':
		                            RM = RM +1.48
		                        elif F == 'K':
		                            RM = RM +1.56
		                        elif F == 'G':
		                            RM = RM +0.22
		                        elif F == 'L':
		                            RM = RM -0.19
		                        elif F == 'o':
		                            RM = RM +0.2305
		                        elif F == 'c':
		                            RM = RM -0.006

		                        RM = "%5.2f" % RM


		                        # Set big surveys

		                        if Cod == '291' or Cod == '644' or Cod == '691' or Cod == '699' or Cod == 'E12' or\
		                            Cod == 'G96' or Cod == '703' or Cod == '704' or Cod == 'F51' or Cod == 'F52' or\
		                            Cod == 'T05' or Cod == 'T08' or Cod == '566':

		                            BS = 1

		                        else:
		                            BS = 0


		                        # Writes file

		                        if abs(RA-RAd) < 1 and abs(DEC-DECd) < 1:
		                            fi.write(year + ' ' + month + ' ' + day + ' ' + str(Delta) + ' ' + str(r) + ' ' +\
		                                     str(alpha) + ' ' + str(RM) + ' ' + str(IO) + ' ' + str(mag) + ' ' + str(F) +\
		                                     ' ' + str(Cod) + ' ' + str(BS) + '\n')

		                        if abs(RA-RAd) < 1 and abs(DEC-DECd) < 1:
		                            fi2.write(year + ' ' + month + ' ' + day + ' ' + str(Delta) + ' ' + str(r) + ' ' +\
		                                     str(alpha) + ' ' + str(RM) + ' ' + str(IO) + ' ' + str(mag) + ' ' + \
		                                      str(BS) + '\n')

		                    except:
		                        continue

	    os.chdir(dir)
